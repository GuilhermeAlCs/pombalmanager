﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pigeon.Model;

namespace Pigeon.Dal.Repository
{
    public class ProvaRepository
    {
        public ProvaRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<Prova> GetAllProvas()
        {
            return contexto.Prova.ToList();
        }
        public List<Prova> GetProvaById(int id)
        {
            return contexto.Prova.Where(c => c.id == id).ToList();
        }
        public List<Prova> GetProvaByCompeticaoId(int id)
        {
            return contexto.Prova.Where(c => c.competicaoId == id).ToList();
        }
        public int NovaProva(Prova x)
        {
            //contexto.Prova.Add(minhaProva);
            ///contexto.SaveChanges();
            var maxid = contexto.Prova.Select(i => i.id).DefaultIfEmpty(0).Max();
            maxid++;
            //"INSERT INTO prova VALUES ({0},{1},{2},{3},{4},{5})";
            contexto.Database.ExecuteSqlCommand(@"INSERT INTO prova VALUES ({0},{1},{2},{3},{4},{5})", maxid, x.latitude, x.longitude, x.tipo, x.descricao, x.competicaoId);

            return maxid;

        }

        public void ExcluirProva(int provaId)
        {
            contexto.Prova.Remove(contexto.Prova.Where(p => p.id.Equals(provaId)).SingleOrDefault());
            contexto.SaveChanges();
        }

    }
}
