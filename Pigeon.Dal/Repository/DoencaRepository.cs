﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pigeon.Model;

namespace Pigeon.Dal.Repository
{
    public class DoencaRepository
    {
        public DoencaRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public Doenca GetDoencaById(int idDoenca)
        {
            return contexto.Doenca.Where(p => p.id.Equals(idDoenca)).SingleOrDefault();
        }

        public List<Doenca> GetAllDoencas()
        {
            return contexto.Doenca.ToList();
        }
        public int NovaDoenca(Doenca minhaDoenca)
        {
            contexto.Doenca.Add(minhaDoenca);
            contexto.SaveChanges();

            return minhaDoenca.id;
        }

        public void ExcluirDoenca(int DoencaID)
        {
            contexto.Doenca.Remove(contexto.Doenca.Where(p => p.id.Equals(DoencaID)).SingleOrDefault());
            contexto.SaveChanges();
        }
        public string AlterarDoenca(Doenca Doenca)
        {
            try
            {
                // contexto.Pombos.Attach(pombo);
                //contexto.Doenca.AddOrUpdate(Doenca);
                contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return Doenca.id.ToString();
        }
    }
}
