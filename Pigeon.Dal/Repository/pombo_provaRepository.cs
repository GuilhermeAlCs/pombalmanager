﻿using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pigeon.Dal.Repository
{
    public class pombo_provaRepository
    {
        public pombo_provaRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<pombo_prova> GetAllItens()
        {
            return contexto.pombo_prova.ToList();
        }
        public List<pombo_prova> GetpomboProvaByPomboId(int id)
        {
            return contexto.pombo_prova.Where(c => c.pomboID == id).ToList();
        }
        public List<pombo_prova> GetpomboProvaByProvaId(int id)
        {
            return contexto.pombo_prova.Where(c => c.provaID == id).ToList();
        }
        public int NovoPomboProva(pombo_prova meuItem)
        {
            try
            {
                contexto.pombo_prova.Add(meuItem);
                contexto.SaveChanges();

                return 1;
            }
            catch (Exception e) {
                return 0;
            }
        }

        public void ExcluirPombo_prova(int pomboId, int provaId)
        {
            contexto.pombo_prova.Remove(contexto.pombo_prova.Where(p => p.provaID.Equals(provaId) && p.pomboID.Equals(pomboId)).SingleOrDefault());
            contexto.SaveChanges();
        }

        public List<TuplaRelatorioProva> GetPombosEmProvas(int idPombo, string anilha, int idProva)
        {
            List<pombo_prova> lista;

            PomboRepository pr = new PomboRepository();
            ProvaRepository pvr = new ProvaRepository();

            if(idPombo == 0 && anilha != "")
            {
                Pombo x = new PomboRepository().GetPomboByAnilha(anilha);
                if (x != null)
                {
                    idPombo = x.Id;
                }
            }
            //se não selecionou prova
            if (idProva == 0)
            {
                //lista = (from P in contexto.pombo_prova
                //         where (idPombo != 0 && P.pomboID == idPombo) ||
                //               anilha != string.Empty && P.pomboID == (pr.GetPomboByAnilha(anilha).Id)
                //         select P).ToList();

                lista = (from P in contexto.pombo_prova
                         where (idPombo != 0 && P.pomboID == idPombo)
                         select P).ToList();
            }
            //se selecionou prova e pombo
            else if (idPombo != 0 && anilha != string.Empty)
            {
                lista = (from P in contexto.pombo_prova
                         where P.pomboID == idPombo  &&
                               idProva == P.provaID
                         select P).ToList();
            }
            // se só selecionou prova
            else
            {
                lista = (from P in contexto.pombo_prova
                         where idProva == P.provaID
                         select P).ToList();
            }

            return (from P in lista
                    select new TuplaRelatorioProva()
                    {
                        Id = P.pomboID,
                        Anilha = pr.GetPomboById(P.pomboID).Anilha,
                        IdProva = P.provaID,
                        Prova = pvr.GetProvaById(P.provaID)[0].descricao,
                        Colocacao = P.Colocacao,
                        Velocidade = new Random().Next(60, 90)
                    }).ToList();
        }

    }
}
