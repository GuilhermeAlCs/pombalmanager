﻿using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pigeon.Dal.Repository
{
    public class CompraRepository
    { 
        public CompraRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<Compra> GetAllCompras()
        {
            return contexto.Compra.ToList();
        }
        public Compra GetCompraById(int Id)
        {
            Compra x = contexto.Compra.First(c => c.id == Id);
            return contexto.Compra.First(c => c.id == Id);
        }
        public int NovaCompra(Compra minhaCompra)
        {
            contexto.Compra.Add(minhaCompra);
            contexto.SaveChanges();

            return minhaCompra.id;
        }

        public int NovoItem(Item meuItem)
        {
            new ItemRepository().NovoItem(meuItem);

            return meuItem.compraID;
        }

        public void ExcluirCompra(int compraID)
        {
            contexto.Compra.Remove(contexto.Compra.Where(p => p.id.Equals(compraID)).SingleOrDefault());
            contexto.SaveChanges();
        }
    }
}
