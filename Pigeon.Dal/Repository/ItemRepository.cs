﻿using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pigeon.Dal.Repository
{
   public  class ItemRepository
    {
        public ItemRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<Item> GetAllItens()
        {
            return contexto.Item.ToList();
        }
        public List<Item> GetItemById(int id)
        {
            return contexto.Item.Where(c => c.id == id).ToList();
        }
        public List<Item> GetItemByCompraId(int id)
        {
            return contexto.Item.Where(c => c.compraID == id).ToList();
        }
        public int NovoItem(Item meuItem)
        {
            contexto.Item.Add(meuItem);
            contexto.SaveChanges();

            return meuItem.id;
        }

        public void ExcluirItem(int ItemId)
        {
            contexto.Item.Remove(contexto.Item.Where(p => p.id.Equals(ItemId)).SingleOrDefault());
            contexto.SaveChanges();
        }
    }
}
