﻿using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pigeon.Dal.Repository
{
    public class pombo_doencaRepository
    {
        public pombo_doencaRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<pombo_doenca> GetAllItens()
        {
            return contexto.pombo_doenca.ToList();
        }
        public List<pombo_doenca> GetpomboDoencaByPomboId(int id)
        {
            return contexto.pombo_doenca.Where(c => c.pomboID == id).ToList();
        }
        public List<pombo_doenca> GetpomboDoencaByDoencaId(int id)
        {
            return contexto.pombo_doenca.Where(c => c.doencaID == id).ToList();
        }
        public int NovoPomboDoenca(pombo_doenca meuItem)
        {
            try
            {
                contexto.pombo_doenca.Add(meuItem);
                contexto.SaveChanges();

                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public void Excluirpombo_doenca(int pomboId, int doencaId)
        {
            contexto.pombo_doenca.Remove(contexto.pombo_doenca.Where(p => p.doencaID.Equals(doencaId) && p.pomboID.Equals(pomboId)).SingleOrDefault());
            contexto.SaveChanges();
        }

    }
}
