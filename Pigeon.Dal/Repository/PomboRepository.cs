﻿using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pigeon.Dal.Repository
{

    public class PomboRepository
    {
        public PomboRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<Pombo> GetAllPombos()
        {
            return contexto.Pombos.ToList();
        }

        public Pombo GetPomboById(int id)
        {
            return contexto.Pombos.Where(p => p.Id.Equals(id)).SingleOrDefault();
        }

        public Pombo GetPomboByAnilha(string anilha)
        {
            return contexto.Pombos.Where(p => p.Anilha == anilha).SingleOrDefault();
        }

        public int NovoPombo(Pombo pombo)
        {
            contexto.Pombos.Add(pombo);
            contexto.SaveChanges();

            return pombo.Id;
        }

        public string AlterarPombo(Pombo pombo)
        {
            try
            {
               // contexto.Pombos.Attach(pombo);
                contexto.Pombos.AddOrUpdate(pombo);
                contexto.SaveChanges();
            }
            catch(Exception ex)
            {
                return ex.Message;
            }

            return pombo.Id.ToString();
        }

        public void ExcluirPombo(int PomboId)
        {
            contexto.Pombos.Remove(contexto.Pombos.Where(p => p.Id.Equals(PomboId)).SingleOrDefault());
            contexto.SaveChanges();
        }

        public ArvoreGenealogica Genealogia(int PomboId)
        {
            ArvoreGenealogica pombos = new ArvoreGenealogica()
            {
                pombo = (from P in contexto.Pombos
                         where P.Id == PomboId
                         select P).SingleOrDefault()
            };

            if (pombos.pombo == null)
                return null;

            pombos.mae = (from P in contexto.Pombos
                      where P.Id == pombos.pombo.MaeId
                      select P).FirstOrDefault();

            pombos.pai = (from P in contexto.Pombos
                      where P.Id == pombos.pombo.PaiId
                      select P).FirstOrDefault();

            //var pombos = (from P in contexto.Pombos
            //              where P.Id == PomboId
            //              select new ArvoreGenealogica
            //              {
            //                  mae = (from P1 in contexto.Pombos
            //                         where P1.Id == P.MaeId
            //                         select P1).FirstOrDefault(),

            //                  pai = (from P1 in contexto.Pombos
            //                         where P1.Id == P.PaiId
            //                         select P1).FirstOrDefault(),

            //                  pombo = GetPomboById(PomboId)
            //              }).FirstOrDefault();

            if (pombos.pai == null && pombos.mae == null)
            {
                return null;
            }

            if (pombos.pai != null)
            {
                pombos.avPat = GetPomboById(pombos.pai.PaiId);
                pombos.avoPat = GetPomboById(pombos.pai.MaeId);
            }
            else
            {
                pombos.pai = new Pombo() { Anilha = "X"};
                pombos.avPat = new Pombo() { Anilha = "X" };
                pombos.avoPat = new Pombo() { Anilha = "X" };
            }

            if (pombos.mae != null)
            {
                pombos.avMat = GetPomboById(pombos.mae.PaiId);
                pombos.avoMat = GetPomboById(pombos.mae.MaeId);
            }
            else
            {
                pombos.mae = new Pombo() { Anilha = "X" };
                pombos.avMat = new Pombo() { Anilha = "X" };
                pombos.avoMat = new Pombo() { Anilha = "X" };
            }

            pombos.filhos = (from P in contexto.Pombos
                             where P.PaiId == PomboId
                             select P).ToList();

            return pombos;
        }
    }
}
