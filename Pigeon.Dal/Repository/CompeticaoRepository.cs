﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pigeon.Model;

namespace Pigeon.Dal.Repository
{
    public class CompeticaoRepository
    {
        public CompeticaoRepository()
        {
            contexto = new DatabaseContext();
        }

        private DatabaseContext contexto;

        public List<Competicao> GetAllCompeticao()
        {
            return contexto.Competicao.ToList();
        }
        public Competicao GetCompeticaoById(int Id)
        {
            Competicao x = contexto.Competicao.First(c => c.id == Id);
            return x;
        }
        public int NovaCompeticao(Competicao minhaCompeticao)
        {
            contexto.Competicao.Add(minhaCompeticao);
            contexto.SaveChanges();

            return minhaCompeticao.id;
        }

        public void ExcluirCompeticao(int CompeticaoId)
        {
            contexto.Competicao.Remove(contexto.Competicao.Where(p => p.id.Equals(CompeticaoId)).SingleOrDefault());
            contexto.SaveChanges();
        }

        public int NovaProva(Prova minhaProva) {

            new ProvaRepository().NovaProva(minhaProva);

            return minhaProva.id;
        }
    }
}
