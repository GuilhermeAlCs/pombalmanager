namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pombo_prova : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.pombo_prova",
                c => new
                    {
                        pomboID = c.Int(nullable: false),
                        provaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.pomboID, t.provaID })
                .ForeignKey("dbo.Pombos", t => t.pomboID, cascadeDelete: true)
                .ForeignKey("dbo.Prova", t => t.provaID, cascadeDelete: true)
                .Index(t => t.pomboID)
                .Index(t => t.provaID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.pombo_prova", "provaID", "dbo.Prova");
            DropForeignKey("dbo.pombo_prova", "pomboID", "dbo.Pombos");
            DropIndex("dbo.pombo_prova", new[] { "provaID" });
            DropIndex("dbo.pombo_prova", new[] { "pomboID" });
            DropTable("dbo.pombo_prova");
        }
    }
}
