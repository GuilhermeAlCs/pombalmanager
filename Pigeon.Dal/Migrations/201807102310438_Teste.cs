namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Teste : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pombos", "Nome", c => c.String());
            AddColumn("dbo.Pombos", "Rank", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pombos", "Rank");
            DropColumn("dbo.Pombos", "Nome");
        }
    }
}
