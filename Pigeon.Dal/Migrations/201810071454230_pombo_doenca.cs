namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pombo_doenca : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.pombo_doenca",
                c => new
                    {
                        pomboID = c.Int(nullable: false),
                        doencaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.pomboID, t.doencaID })
                .ForeignKey("dbo.Doenca", t => t.doencaID, cascadeDelete: true)
                .ForeignKey("dbo.Pombos", t => t.pomboID, cascadeDelete: true)
                .Index(t => t.pomboID)
                .Index(t => t.doencaID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.pombo_doenca", "pomboID", "dbo.Pombos");
            DropForeignKey("dbo.pombo_doenca", "doencaID", "dbo.Doenca");
            DropIndex("dbo.pombo_doenca", new[] { "doencaID" });
            DropIndex("dbo.pombo_doenca", new[] { "pomboID" });
            DropTable("dbo.pombo_doenca");
        }
    }
}
