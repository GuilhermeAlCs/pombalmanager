namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Doenca : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Prova", new[] { "CompeticaoId" });
            CreateIndex("dbo.Prova", "competicaoId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Prova", new[] { "competicaoId" });
            CreateIndex("dbo.Prova", "CompeticaoId");
        }
    }
}
