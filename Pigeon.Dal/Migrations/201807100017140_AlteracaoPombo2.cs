namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteracaoPombo2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Pombos", "Nome");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pombos", "Nome", c => c.String());
        }
    }
}
