namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Outras : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pombos", "PaiId", c => c.Int(nullable: false));
            AddColumn("dbo.Pombos", "MaeId", c => c.Int(nullable: false));
            DropColumn("dbo.Pombos", "Nome");
            DropColumn("dbo.Pombos", "Rank");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pombos", "Rank", c => c.Int(nullable: false));
            AddColumn("dbo.Pombos", "Nome", c => c.String());
            DropColumn("dbo.Pombos", "MaeId");
            DropColumn("dbo.Pombos", "PaiId");
        }
    }
}
