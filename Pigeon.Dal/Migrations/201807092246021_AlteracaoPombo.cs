namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteracaoPombo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pombos", "Nome", c => c.String());
            AddColumn("dbo.Pombos", "DataNasc", c => c.DateTime(nullable: false));
            AddColumn("dbo.Pombos", "Sexo", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pombos", "Sexo");
            DropColumn("dbo.Pombos", "DataNasc");
            DropColumn("dbo.Pombos", "Nome");
        }
    }
}
