namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pombos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Anilha = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pombos");
        }
    }
}
