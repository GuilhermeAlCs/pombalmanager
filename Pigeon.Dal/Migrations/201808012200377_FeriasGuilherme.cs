namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FeriasGuilherme : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Competicao",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ano = c.DateTime(nullable: false),
                        nome = c.String(maxLength: 15),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Prova",
                c => new
                    {
                        id = c.Int(nullable: false),
                        latitude = c.String(),
                        longitude = c.String(),
                        Tipo = c.String(),
                        Descricao = c.String(maxLength: 15),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Competicao", t => t.id)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.Compra",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false),
                        descricao = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Doenca",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        valor = c.Double(nullable: false),
                        Nome = c.String(maxLength: 25),
                        descricao_sintomas = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Item",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        valor = c.Double(nullable: false),
                        Nome = c.String(maxLength: 20),
                        compraID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Compra", t => t.compraID, cascadeDelete: true)
                .Index(t => t.compraID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Item", "compraID", "dbo.Compra");
            DropForeignKey("dbo.Prova", "id", "dbo.Competicao");
            DropIndex("dbo.Item", new[] { "compraID" });
            DropIndex("dbo.Prova", new[] { "id" });
            DropTable("dbo.Item");
            DropTable("dbo.Doenca");
            DropTable("dbo.Compra");
            DropTable("dbo.Prova");
            DropTable("dbo.Competicao");
        }
    }
}
