namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class provaIdFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Prova", "id", "dbo.Competicao");
            DropIndex("dbo.Prova", new[] { "id" });
            DropPrimaryKey("dbo.Prova");
            AddColumn("dbo.Prova", "competicaoId", c => c.Int(nullable: false));
            AlterColumn("dbo.Prova", "id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Prova", "id");
            CreateIndex("dbo.Prova", "competicaoId");
            AddForeignKey("dbo.Prova", "competicaoId", "dbo.Competicao", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prova", "competicaoId", "dbo.Competicao");
            DropIndex("dbo.Prova", new[] { "competicaoId" });
            DropPrimaryKey("dbo.Prova");
            AlterColumn("dbo.Prova", "id", c => c.Int(nullable: false));
            DropColumn("dbo.Prova", "competicaoId");
            AddPrimaryKey("dbo.Prova", "id");
            CreateIndex("dbo.Prova", "id");
            AddForeignKey("dbo.Prova", "id", "dbo.Competicao", "id");
        }
    }
}
