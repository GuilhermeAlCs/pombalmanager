namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HoadaChegada : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.pombo_prova", "DataHoraChegada", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.pombo_prova", "DataHoraChegada", c => c.DateTime(nullable: false));
        }
    }
}
