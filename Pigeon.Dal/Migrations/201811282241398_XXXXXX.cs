namespace Pigeon.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class XXXXXX : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.pombo_prova", "Colocacao", c => c.Int(nullable: false));
            AddColumn("dbo.pombo_prova", "DataHoraChegada", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.pombo_prova", "DataHoraChegada");
            DropColumn("dbo.pombo_prova", "Colocacao");
        }
    }
}
