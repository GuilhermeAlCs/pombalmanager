﻿using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pigeon.Dal
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext()
           : base("Conexao")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DatabaseContext>(null);
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Pombo> Pombos { get; set; }
        public virtual DbSet<Doenca> Doenca { get; set; }
        public virtual DbSet<Competicao> Competicao { get; set; }
        public virtual DbSet<Prova> Prova { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<Compra> Compra { get; set; }
        public virtual DbSet<pombo_prova> pombo_prova { get; set; }
        public virtual DbSet<pombo_doenca> pombo_doenca { get; set; }
    }
}
