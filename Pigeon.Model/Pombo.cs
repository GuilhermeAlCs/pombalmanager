﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pigeon.Model
{
    [Table("Pombos")]
    public class Pombo
    {
        [Key]
        public int Id { get; set; }

        public string Anilha { get; set; }

        public DateTime DataNasc { get; set; }

        [NotMapped]
        public int Rank { get; set; }

        [MaxLength(1)]
        public string Sexo { get; set; }

        public int PaiId { get; set; }

        public int MaeId { get; set; }

    }
}
