﻿using System;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pigeon.Model
{
    public class pombo_doenca
    {
        [Key, Column(Order = 0), ForeignKey("Pombo")]
        public int pomboID { get; set; }

        public virtual Pombo Pombo { get; set; }

        [Key, Column(Order = 1), ForeignKey("Doenca")]
        public int doencaID { get; set; }

        public virtual Doenca Doenca { get; set; }
    }
}