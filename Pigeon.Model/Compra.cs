﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pigeon.Model
{
    [Table("Compra")]
    public class Compra
    {
        [Key]
        public int id { get; set; }
        public DateTime data { get; set; }
        [MaxLength(20)]
        public string descricao { get; set; }

    }
}
