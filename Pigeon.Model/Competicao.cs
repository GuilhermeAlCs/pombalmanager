﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pigeon.Model
{
    [Table("Competicao")]
    public class Competicao
    {
        [Key]
        public int id { get; set; }
        public DateTime ano { get; set; }
        [MaxLength(15)]
        public string nome { get; set; }

    }
}
