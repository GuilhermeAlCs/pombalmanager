﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pigeon.Model
{
    public class ArvoreGenealogica
    {
        public Pombo pombo { get; set; }
        public Pombo mae { get; set; }
        public Pombo pai { get; set; }
        public Pombo avPat { get; set; }
        public Pombo avoPat { get; set; }
        public Pombo avMat { get; set; }
        public Pombo avoMat { get; set; }
        public List<Pombo> filhos { get; set; }
    }
}
