﻿using System;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pigeon.Model
{
    public class pombo_prova
    {

        [Key, Column(Order = 0), ForeignKey("Pombo")]
        public int pomboID { get; set; }

        public virtual Pombo Pombo { get; set; }

        [Key, Column(Order = 1), ForeignKey("Prova")]
        public int provaID { get; set; }

        public virtual Prova Prova { get; set; }

        public int Colocacao { get; set; }

        public DateTime? DataHoraChegada {get; set;}
    }
}