﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pigeon.Model
{
    [Table("Item")]
    public class Item
    {
        [Key]
        public int id { get; set; }
        public double valor { get; set; }
        [MaxLength(20)]
        public string Nome { get; set; }

        [ForeignKey("Compra")]
        public int compraID { get; set; }

        public virtual Compra Compra { get; set; }

    }
}
