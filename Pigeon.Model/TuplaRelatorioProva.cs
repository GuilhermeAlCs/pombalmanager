﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pigeon.Model
{
    public class TuplaRelatorioProva
    {
        public int Id { get; set; }
        public string Anilha { get; set; }
        public int IdProva { get; set; }
        public string Prova { get; set; }
        public int Colocacao { get; set; }
        public int Velocidade { get; set; }
    }
}
