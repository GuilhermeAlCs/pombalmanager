﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pigeon.Model
{
    [Table("Logins")]
    public class Login
    {
        [Key]
        public int Id { get; set; }

        public string Usuario { get; set; }

        public string Senha { get; set; }

        public bool Reset { get; set; }
    }
}
