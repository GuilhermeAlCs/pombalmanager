﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pigeon.Model
{
    [Table("Criadores")]
    public class Criador
    {
        [Key]
        public int Id { get; set; }

        public string Nome { get; set; }

        [ForeignKey("Endereco")]
        public int EnderecoId { get; set; }

        [ForeignKey("Login")]
        public int LoginId { get; set; }

        public virtual Endereco Endereco { get; set; }

        public virtual Login Login { get; set; }
    }
}
