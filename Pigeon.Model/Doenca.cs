﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pigeon.Model
{
    [Table("Doenca")]
    public class Doenca
    {
        [Key]
        public int id { get; set; }
        public double valor { get; set; }
        [MaxLength(25)]
        public string Nome { get; set; }
        public string descricao_sintomas { get; set; }

    }
}
