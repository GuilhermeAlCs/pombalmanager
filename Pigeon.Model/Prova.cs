﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pigeon.Model
{
    [Table("Prova")]
    public class Prova
    {
        [Key]
        public int id { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string tipo { get; set; }
        [MaxLength(15)]
        public string descricao { get; set; }
        [ForeignKey("minhaCompeticao")]
        public int competicaoId { get; set; }
        public virtual Competicao minhaCompeticao { get; set; } 
    }
}
