﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pigeon.Dal.Repository;
using Pigeon.Model;

namespace Pigeon.MVC.Controllers
{
    public class CompeticaoController : Controller
    {
        // GET: Competicao
        public ActionResult Index()
        {
            return View(new CompeticaoRepository().GetAllCompeticao());
        }

        [HttpGet]
        [Route("Competicao/{id}")]
        public ActionResult Competicao(int id)
        {
            Competicao minhaCompeticao = new CompeticaoRepository().GetCompeticaoById(id);
            List<Prova> meuItens = new ProvaRepository().GetProvaByCompeticaoId(id);
            ViewBag.meusItems = meuItens;

            ViewBag.minhaCompeticao = minhaCompeticao;


            return View("Detail");
        }
        //Adicionar item
        [HttpPost]
        [Route("Competicao/addprova")]
        public ActionResult adcionarItem(int idCompeticao, Prova itemGravar)
        {
            try
            {

                itemGravar.competicaoId = idCompeticao;
                new ProvaRepository().NovaProva(itemGravar);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(1);
        }

        [HttpPost]
        [Route("Competicao/CadastrarCompeticao")]
        public ActionResult CadastrarCompeticao(Competicao minhaCompeticao)
        {
            try
            {
                new CompeticaoRepository().NovaCompeticao(minhaCompeticao);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(minhaCompeticao.id);
        }

        [HttpPost]
        [Route("Competicao/ExcluirCompeticao")]
        public ActionResult ExcluirCompeticao(int id)
        {
            try
            {
                new CompeticaoRepository().ExcluirCompeticao(id);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(id);
        }

        //Adicionar item
        [HttpPost]
        [Route("competicao/excluirProva")]
        public ActionResult excluirProva(int id)
        {
            try
            {
                new ProvaRepository().ExcluirProva(id);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(1);
        }

    }
}