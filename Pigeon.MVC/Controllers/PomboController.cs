﻿using Pigeon.Dal.Repository;
using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pigeon.MVC.Controllers
{
    public class PomboController : Controller
    {

        // GET: Pombal
        public ActionResult Index()
        {
            return View(new PomboRepository().GetAllPombos());
        }

        [HttpGet]
        [Route("pombos/{id}", Name = "VisualizaPombo")]
        public ActionResult Pombos(int id)
        {
            DoencaRepository DR = new DoencaRepository();
            ViewBag.proximasProvas = new ProvaRepository().GetAllProvas();
            ViewBag.doencasCadastradas = DR.GetAllDoencas();
            ViewBag.provasDoPombo = buscarProvasPombo(id);
            ViewBag.DoencasDoPombo = (from D in new pombo_doencaRepository().GetpomboDoencaByPomboId(id)
                                      select DR.GetDoencaById(D.doencaID)).ToList();
            return View(new PomboRepository().GetPomboById(id));
        }

        [HttpPost]
        public ActionResult CadastrarPombo(Pombo pombo)
        {
            new PomboRepository().NovoPombo(pombo).ToString();
            return View("_NovoPombo", pombo);
        }


        [HttpPost]
        public ActionResult AlterarPombo(Pombo pombo)
        {
            string msg = new PomboRepository().AlterarPombo(pombo).ToString();
            return Json(msg);
        }

        [HttpPost]
        public ActionResult ExcluirPombo(int Id)
        {
            try
            {
                new PomboRepository().ExcluirPombo(Id);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(Id);
        }

        //pombo_prova
        [HttpPost]
        public ActionResult registrarPomboEmProva(int idPombo, int idProva) {
            int result = 0;
            try
            {
                pombo_prova meuPombo = new pombo_prova()
                {

                };
                meuPombo.pomboID = idPombo;
                meuPombo.provaID = idProva;
                new pombo_provaRepository().NovoPomboProva(meuPombo);
                result = 1;
            }
            catch (Exception e) {
                result = 0;
            }
            return Json(result);
        }

        public List<Prova> buscarProvasPombo(int idPombo) {
            List<Prova> resultado =  new List<Prova>();
            List<pombo_prova> provasDoPombo = new pombo_provaRepository().GetpomboProvaByPomboId(idPombo);
            foreach (pombo_prova prova in provasDoPombo)
            {
                resultado.Add(new ProvaRepository().GetProvaById(prova.provaID).First());
            }
            return resultado;
        }
        //pombo doenca
        [HttpPost]
        public ActionResult registrarDoencaEmPombo(int idPombo, int idDoenca)
        {
            int result = 0;
            try
            {
                pombo_doenca meuPombo = new pombo_doenca();
                meuPombo.pomboID = idPombo;
                meuPombo.doencaID = idDoenca;
                new pombo_doencaRepository().NovoPomboDoenca(meuPombo);
                result = 1;
            }
            catch (Exception e)
            {
                result = 0;
            }
            return Json(result);
        }

        public List<Doenca> buscarDoencasPombo(int idPombo)
        {
            List<Doenca> resultado = new List<Doenca>();
            List<pombo_doenca> DoencasDoPombo = new pombo_doencaRepository().GetpomboDoencaByPomboId(idPombo);
            foreach (pombo_doenca prova in DoencasDoPombo)
            {
                resultado.Add(new DoencaRepository().GetDoencaById(prova.doencaID));
            }
            return resultado;
        }
    }
}