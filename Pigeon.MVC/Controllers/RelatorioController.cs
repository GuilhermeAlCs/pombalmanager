﻿using Pigeon.Dal.Repository;
using Pigeon.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pigeon.MVC.Controllers
{
    public class RelatorioController : Controller
    {
        // GET: Relatorio
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Genealogia(int idPombo, string anilha)
        {
            ArvoreGenealogica AG;
            PomboRepository Pr = new PomboRepository();

            if (idPombo != 0)
            {
                 AG = Pr.Genealogia(idPombo);
            }
            else
            {
                Pombo pombo = Pr.GetPomboByAnilha(anilha);
                if (pombo == null)
                    AG = null;
                else  AG = Pr.Genealogia(pombo.Id);
            }
                
             
            /*
             new ArvoreGenealogica()
            {
                mae = new Pombo() { Id = 0, Anilha = "XXX XXXX"  },
                pai = new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                avMat = new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                avoMat = new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                avPat = new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                avoPat = new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                pombo = new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                filhos = new List<Pombo>() {
                    new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                    new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                    new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                    new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                    new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                    new Pombo() { Id = 0, Anilha = "XXX XXXX" },
                },
                
            };*/

            if (AG == null)
            {
                return Json("Nenhum traço de genealogia encontrado");
            }
            return Json(AG);
        }

        [HttpGet]
        [Route("Relatorios")]
        public ActionResult Provas()
        {
            List<Prova> Provas; /*= new List<Prova>()
            {
                new Prova()
                {
                    id = 0,
                    descricao = "Prova do  Cabuloso"
                },
                new Prova()
                {
                    id = 0,
                    descricao = "Prova do  Cabuloso"
                },
                new Prova()
                {
                    id = 0,
                    descricao = "Prova do  Cabuloso"
                }
            }; */

            Provas = new ProvaRepository().GetAllProvas();
            return View(Provas);
        }

        [HttpPost]
        public ActionResult _BuscaPorPombo(int idPombo, string anilha, int idProva)
        {
            List<TuplaRelatorioProva> Tuplas =  new pombo_provaRepository().GetPombosEmProvas(idPombo, anilha, idProva);

            /*Tuplas = new List<TuplaRelatorioProva>() {
                new TuplaRelatorioProva()
                {
                    Id = 0,
                    Anilha = "AAA AAAA",
                    Prova = "Sapucaí do Brejo",
                    Colocacao = 2,
                    Velocidade = 74
                },
                 new TuplaRelatorioProva()
                {
                    Id = 0,
                    Anilha = "AAA AAAA",
                    Prova = "Sapucaí do Brejo",
                    Colocacao = 2,
                    Velocidade = 74
                },
                  new TuplaRelatorioProva()
                {
                    Id = 0,
                    Anilha = "AAA AAAA",
                    Prova = "Sapucaí do Brejo",
                    Colocacao = 2,
                    Velocidade = 74
                },
                   new TuplaRelatorioProva()
                {
                    Id = 0,
                    Anilha = "AAA AAAA",
                    Prova = "Sapucaí do Brejo",
                    Colocacao = 2,
                    Velocidade = 74
                }
            };*/

            return View(Tuplas);
        }


    }
}