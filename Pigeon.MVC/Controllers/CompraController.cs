﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pigeon.Dal.Repository;
using Pigeon.Model;

namespace Pigeon.MVC.Controllers
{
    public class CompraController : Controller
    {

        // GET: Compra
        public ActionResult Index()
        {
            return View(new CompraRepository().GetAllCompras().ToList());
        }
        [HttpGet]
        [Route("compra/{id}")]
        public ActionResult Compras(int id)
        {
            Compra minhaCompra = new CompraRepository().GetCompraById(id);
            List<Item> meuItens = new ItemRepository().GetItemByCompraId(id);
            ViewBag.meusItems = meuItens;
            Double total = 0;
            if (meuItens != null) {
                foreach (var item in meuItens)
                {
                    total += item.valor;
                }
            }

            ViewBag.minhaCompra = minhaCompra;

            ViewBag.subtotal = total;
            return View("Detail");
        }
        //Adicionar item
        [HttpPost]
        [Route("compra/{idCompra}/additem")]
        public ActionResult adcionarItem(int idCompra, Item itemGravar)
        {
            try
            {
                itemGravar.compraID = idCompra;
                new CompraRepository().NovoItem(itemGravar);
            }
            catch (Exception ex) {
                return Json(ex.Message);
            }
            return Json(1);
        }
        //Adicionar item
        [HttpPost]
        [Route("compra/{idItem}/excluirItem")]
        public ActionResult excluirItem(int id)
        {
            try
            {
                new ItemRepository().ExcluirItem(id);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(1);
        }

        [HttpPost]
        [Route("Compra/CadastrarCompra")]
        public ActionResult CadastrarCompra(Compra compra)
        {
            try
            {
                new CompraRepository().NovaCompra(compra).ToString();
            }
            catch (Exception ex) {
                return Json(ex.Message);
            }
            return Json(compra.id);
        }

        [HttpPost]
        [Route("Compra/ExcluirCompra/{id}")]
        public ActionResult ExcluirCompra(int id)
        {
            try
            {
                new CompraRepository().ExcluirCompra(id);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(id);
        }
    }
}