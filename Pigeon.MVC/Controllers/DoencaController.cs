﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pigeon.Dal.Repository;
using Pigeon.Model;

namespace Pigeon.MVC.Controllers
{
    public class DoencaController : Controller
    {
        // GET: Doenca
        public ActionResult Index()
        {
            return View(new DoencaRepository().GetAllDoencas());
        }


        [HttpGet]
        [Route("doenca/{id}")]
        public ActionResult Doenca(int id)
        {
            ViewBag.doenca = new DoencaRepository().GetDoencaById(id);
            return View("Detail");
        }

        [HttpPost]
        [Route("doenca/cadastradoenca")]
        public ActionResult CadastraDoenca(Doenca minhaDoenca)
        {
            try
            {
                new DoencaRepository().NovaDoenca(minhaDoenca);
            }
            catch (Exception ex) {
                return Json(ex.Message);
            }
            return Json(1);
        }


        [HttpPost]
        public ActionResult AlterarDoenca(Doenca Doenca)
        {
            string msg = new DoencaRepository().AlterarDoenca(Doenca).ToString();
            return Json(msg);
        }

        [HttpPost]
        public ActionResult ExcluirDoenca(int Id)
        {
            try
            {
                new DoencaRepository().ExcluirDoenca(Id);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(Id);
        }
    }
}