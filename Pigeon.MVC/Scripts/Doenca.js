﻿$(function () {
    $("#tbxBuscar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".table tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });

});

function ModalCadastroDoenca() {
    var html =
        `<div class="row">

        <div class="form-group col-md-6">
            <label for="descricao">Nome:</label>
            <input type="text" class="form-control" id="nome" placeholder="Fagocitose">
            <div class="invalid-feedback">
                Digite um nome para a doença.
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="descricao">Descrição:</label>
            <input type="text" class="form-control" id="descricao" placeholder="Sacos de milho e ervilhas">
            <div class="invalid-feedback">
                Digite uma descrição e verifique o tamanho dela.
            </div>
        </div>

    </div>`;

    ModalShow("Cadastrar Doenca", "CadastraDoencaPre();", "", "Cadastrar", "Cancelar", html);
}

function ModalExclusaoCompra(icon) {
    var html = "Tem certeza que deseja excluir o cadastro da doenca '" + $($(icon).closest("tr").find("td")[0]).text() + "'?";

    ModalShow("Exclusão de Doenca", "ExcluirDoencaPre(" + $(icon).closest("tr").attr("id").split("-")[1] + ");", "", "Sim", "Não", html);
}


function ValidacaoDoenca() {
    var valido = true;
    var campo = $("#nome");
    if (campo.val() === "") {
        InvalidarPreenchimento(campo, "Nome Inválido.");
        valido = false;
    }
    campo = $("#descricao");
    if (campo.val() === "") {
        InvalidarPreenchimento(campo, "Descricao Inválida.");
        valido = false;
    }

    return valido;
}

function CadastroDoencaPos(id) {
    StopProgressBar();
    if (!$.isNumeric(id)) {
        Mensagem("erro", "Ocorreu um erro inesperado. Cadastro não efetuado!");
    }
    else {
        Mensagem("sucesso", "Cadastrado com Sucesso!");
    }
}

function ExcluirDoencaPos(id) {
    StopProgressBar();
    if (!$.isNumeric(id)) {
        Mensagem("erro", "Ocorreu um erro inesperado. Exclusão não efetuada!");
    }
    else {
        $("#Pombo-" + id).remove();
        Mensagem("sucesso", "Excluído com Sucesso!");
    }
}

function InvalidarPreenchimento(campo, texto) {
    campo.addClass("is-invalid");
    $(campo.parent().find("div")[0]).text(texto);
}