﻿$(function () {
    $("#tbxBuscar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".table tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

});

function ModalCadastroCompeticao() {
    var html =
        `<div class="row">

        <div class="form-group col-md-6">
            <label for="data">Data:</label>
            <input type="date" class="form-control" id="data" placeholder="11/11/2011">
            <div class="invalid-feedback">
                Digite uma data válida.
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="nome">Descricao:</label>
            <input type="text" class="form-control" id="nome" placeholder="Sacos de milho e ervilhas">
            <div class="invalid-feedback">
                Digite uma descrição e verifique o tamanho dela.
            </div>
        </div>

    </div>`;

    ModalShow("Cadastrar Competicao", "CadastrarCompeticaoPre();", "", "Cadastrar", "Cancelar", html);
}

function ModalExclusaoCompeticao(icon) {
    var html = "Tem certeza que deseja excluir o cadastro do Competicao '" + $($(icon).closest("tr").find("td")[0]).text() + "'?";

    ModalShow("Exclusão de Competicao", "ExcluirCompeticaoPre(" + $(icon).closest("tr").attr("id").split("-")[1] + ");", "", "Sim", "Não", html);
}

function ValidacaoCompeticao() {
    var valido = true;
    var campo = $("#data");
    if (campo.val() === "") {
        InvalidarPreenchimento(campo, "Data Inválida.");
        valido = false;
    }
    campo = $("#nome");
    if (campo.val() === "") {
        InvalidarPreenchimento(campo, "Descricao Inválida.");
        valido = false;
    }

    return valido;
}

function CadastrarCompeticaoPos(id) {
    StopProgressBar();
    if (!$.isNumeric(id)) {
        Mensagem("erro", "Ocorreu um erro inesperado. Cadastro não efetuado!")
    }
    else {
        Mensagem("sucesso", "Cadastrado com Sucesso!")
    }
}

function ExcluirCompeticaoPos(id) {
    StopProgressBar();
    if (!$.isNumeric(id)) {
        Mensagem("erro", "Ocorreu um erro inesperado. Exclusão não efetuada!")
    }
    else {
        $("#Pombo-" + id).remove();
        Mensagem("sucesso", "Excluído com Sucesso!")
    }
}

function InvalidarPreenchimento(campo, texto) {
    campo.addClass("is-invalid");
    $(campo.parent().find("div")[0]).text(texto);
}