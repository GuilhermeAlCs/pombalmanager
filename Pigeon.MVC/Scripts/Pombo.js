﻿$(function () {
    $("#tbxBuscar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".table tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

});

function ModalCadastroPombo()
{
    var html =
        `<div class="row">

        <div class="form-group col-md-6">
            <label for="tbxAnilha">Anilha:</label>
            <input type="text" class="form-control" id="tbxAnilha" placeholder="XXX 0000">
            <div class="invalid-feedback">
                Digite uma anilha válida.
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="ddlSexo">Sexo:</label>
            <select id="ddlSexo" class="form-control">
                <option value="0" selected disabled>Selecionar</option>
                <option value="M" selected>Macho</option>
                <option value="F">Femea</option>
            </select>
            <div class="invalid-feedback">
                Selecione um dia do mês.
            </div>
        </div>

    </div>
    <div class="row">

        <div class="form-group col-md-6">
            <label for="tbxDataNasc">Data Nascimento:</label>
            <input type="date" class="form-control" id="tbxDataNasc">
            <div class="invalid-feedback">
                Digite uma data válida.
            </div>
        </div>

    </div>`;

    ModalShow("Cadastrar Pombo", "CadastrarPomboPre();", "","Cadastrar","Cancelar", html);
}

function ModalExclusaoPombo(icon) {

    var e = window.event;                // Get the window event
    e.cancelBubble = true;                       // IE Stop propagation
    if (e.stopPropagation) e.stopPropagation();  // Other Broswers

    var html = "Tem certeza que deseja excluir o cadastro do pombo '" + $($(icon).closest("tr").find("td")[0]).text() + "'?";

    ModalShow("Exclusão de Pombo", "ExcluirPomboPre(" + $(icon).closest("tr").attr("id").split("-")[1] + ");", "","Sim","Não",html);
}

function ValidacaoPombo() {
    var valido = true;
    var campo = $("#tbxAnilha");
    if (campo.val() == "") {
        InvalidarPreenchimento(campo,"Anilha Inválida.");
        valido = false;
    }
    campo = $("#tbxDataNasc");
    if (campo.val() == "") {
        InvalidarPreenchimento(campo,"Data Inválida.");
        valido = false;
    }
    campo = $("#ddlSexo option:selected");
    if (campo.val() == 0) {
        InvalidarPreenchimento(campo,"Selecione um sexo.");
        valido = false;
    }
    return valido;
}

function CadastrarPomboPos(html)
{
    StopProgressBar();
    if ($.trim(html).indexOf("<tr") == -1)
    {
        Mensagem("erro", "Ocorreu um erro inesperado. Cadastro não efetuado!")
    }
    else {
        $($("tbody")[0]).html($($("tbody")[0]).html() + html);
        Mensagem("sucesso","Cadastrado com Sucesso!")
    }
}

function ExcluirPomboPos(id) {
    StopProgressBar();
    if (!$.isNumeric(id)) {
        Mensagem("erro", "Ocorreu um erro inesperado. Exclusão não efetuada!")
    }
    else {
        $("#Pombo-" + id).remove();
        Mensagem("sucesso", "Excluído com Sucesso!")
    }
}

function AlterarPomboPos(id)
{
    if ($.isNumeric(id))
    {
        $("#"+id).parent().parent().find("input,select").prop("disabled", true);
        $("#" + id).attr("src", "../../Content/imgs/icons/edit.png");
        StopProgressBar();

        $("#" + id).attr("onclick", "LiberarAlteracaoPombo(this);");

        Mensagem("sucesso", "Alterado com Sucesso!")        
    }
    else {
        StopProgressBar();
        Mensagem("erro", "Ocorreu um erro inesperado. Alteração não efetuada!")
    }
}

function InvalidarPreenchimento(campo, texto)
{
    campo.addClass("is-invalid");
    $(campo.parent().find("div")[0]).text(texto);
}

function LiberarAlteracaoPombo(icon)
{
    icon = $(icon);
    if (icon.attr("src").indexOf("save") == -1)
    {
        icon.attr("src", "../../Content/imgs/icons/save.png");

        icon.attr("onclick", "AlteracaoPomboPre("+icon.attr("id")+");");

        icon.parent().parent().find("input,select").prop("disabled", false);
    }
}



