﻿function ModalShow(Title, FunctionConfirm, FunctionCancel, TextConfirmButton, TextCancelButton, HTML) {

    $("#ModalTitle").text(Title);


    $("#modalBtnConfirmar").attr("onclick", FunctionConfirm);

    if (TextConfirmButton != "")
    {
        $("#modalBtnConfirmar").text(TextConfirmButton);
    }

    if (FunctionCancel != "")
    {
        $("#modalBtnCancelar").attr("onclick", FunctionCancel);
    }

    if (TextCancelButton != "") {
        $("#modalBtnCancelar").text(TextCancelButton);
    }

    $($("#Modal").find(".container-fluid")[0]).html(HTML);

    $("#Modal").modal("show");
}


function ProgressBar() {
    StopScroll();
    $('body').waitMe({
        effect: 'ios',
        text: 'Quase Pronto...',
        color: '#007bff'
    });
};
function StopProgressBar() {
    StartScroll();
    $('body').waitMe('hide');
};

var Scroll;
function StopScroll() {
    //$("body").removeClass("start-scroll");
    //$("body").addClass("stop-scroll");
};

function StartScroll() {
    //$("body").removeClass("stop-scroll");
    //$("body").addClass("start-scroll");
};


function Mensagem(tipo, texto) {
    alertify.set('notifier', 'position', 'top-center');

    tipo = tipo.toLowerCase();

    if (tipo == "sucesso")
        alertify.success(texto);
    else if (tipo == "erro")
        alertify.error(texto);
    else if (tipo == "info")
        alertify.notify(texto);
    else if (tipo == "alerta")
        alertify.warning(texto);

}